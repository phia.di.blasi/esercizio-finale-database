CREATE DATABASE developers;

/** Creazione di tutte le tabelle che compongono il db**/
CREATE TABLE developers.sviluppatore(
	id int auto_increment primary key,
    nome varchar (25) NOT NULL,
    cognome varchar (25) NOT NULL,
    codice_fiscale varchar (16) NOT NULL,
    data_nascita date NOT NULL,
    indirizzo_residenza varchar (50) NOT NULL,
    email varchar (50) NOT NULL,
    telefono numeric,
    cellulare numeric NOT NULL,
    cv_text text,
    cv_pdf mediumblob,
    constraint cv_caricato CHECK ((cv_text IS NOT NULL) or (cv_pdf IS NOT NULL))
);

CREATE TABLE developers.skill(
	id int auto_increment primary key,
    nome_skill varchar (25) NOT NULL
);

CREATE TABLE developers.progetto(
	id int auto_increment primary key,
    nome_progetto varchar (50) NOT NULL,
    coordinatore int NOT NULL,
    foreign key (coordinatore) references developers.sviluppatore(id)
);

CREATE TABLE developers.tipologia_task(
	id int auto_increment primary key,
    nome_tipologia varchar (20) NOT NULL
);

CREATE TABLE developers.task(
	id int auto_increment primary key,
    numero_max_collaboratori int NOT NULL,
    data_inizio date NOT NULL,
    data_fine date NOT NULL,
    titolo varchar (50) NOT NULL,
    descrizione text NOT NULL,
    stato_task boolean NOT NULL,
    tipologia int NOT NULL,
    progetto int NOT NULL,
    foreign key (tipologia) references developers.tipologia_task(id),
    foreign key (progetto) references developers.progetto(id)
);

CREATE TABLE developers.allegato(
	id int auto_increment primary key,
    nome_file varchar (50) NOT NULL,
    tipologia varchar (30) NOT NULL,
    data_modifica date NOT NULL,
    contenuto longblob NOT NULL,
    modificato_da int NOT NULL,
    task int NOT NULL,
    foreign key (modificato_da) references developers.sviluppatore(id),
    foreign key (task) references developers.task(id)
);

CREATE TABLE developers.messaggio(
	id int auto_increment primary key,
    stato_messaggio boolean NOT NULL,
    tipologia varchar (30) NOT NULL,
    testo text NOT NULL,
    autore int NOT NULL,
    progetto int NOT NULL,
    foreign key (autore) references developers.sviluppatore(id),
    foreign key (progetto) references developers.progetto(id)
);

CREATE TABLE developers.sviluppatore_skill(
	livello_competenza int NOT NULL,
	sviluppatore int NOT NULL,
    skill int NOT NULL,
	foreign key (sviluppatore) references developers.sviluppatore(id),
    foreign key (skill) references developers.skill(id),
    constraint range_competenza CHECK ( livello_competenza >=1 and livello_competenza <= 10)
);

CREATE TABLE developers.tipo_task_skill(
	tipologia_task int NOT NULL,
    skill int NOT NULL,
	foreign key (tipologia_task) references developers.tipologia_task(id),
    foreign key (skill) references developers.skill(id)
);

CREATE TABLE developers.task_skill(
	competenza_minima int NOT NULL,
	task int NOT NULL,
    skill int NOT NULL,
	foreign key (task) references developers.task(id),
    foreign key (skill) references developers.skill(id),
    constraint range_competenza_minima CHECK ( competenza_minima >=1 and competenza_minima <= 10)
);

CREATE TABLE developers.sviluppatore_task(
	valutazione int,
	task int NOT NULL,
    sviluppatore int NOT NULL,
	foreign key (task) references developers.task(id),
    foreign key (sviluppatore) references developers.sviluppatore(id),
	constraint valutazione CHECK ( valutazione >=1 and  valutazione <=5  )
);

/**
 uso dei trigger per gestire l'inserimento e l'update delle skill, verificandone l'appartenenza alla tipologia di task giusto
 utilizzo del trigger al posto del check poichè era necessario fare una verifica su una condizione molto complessa non supportata dal check in mysql
**/
DELIMITER //

CREATE TRIGGER developers.controllo_inserimento_skill_tipologia_corretta_task
before insert on developers.task_skill
for each row
BEGIN
	-- recupero l'id del tipo task dalla tab task dato il task che sto provando ad inserire --
	SET @tipo_task = (SELECT task.tipologia FROM developers.task WHERE task.id = new.task);
    -- se la skill che sto provando ad inserire non è nella lista di quelle associate alla tipologia che ho recuperato lancio l'errore --
	if (new.skill) NOT IN (SELECT tipo_task_skill.skill FROM developers.tipo_task_skill WHERE tipo_task_skill.tipologia_task = @tipo_task)	then 
		-- uso il concat per avere una stringa che mi aiuti a capire dove sta l'errore --
		SET @errorMsg = CONCAT ('La skill ', new.skill, ' selezionata non va bene per il tipo di task ' , @tipo_task, 'per il task numero ', new.task);
        -- lancia l'errore con un codice errore indicato in sqlstate ed un messaggio indicato in mesage text --
		signal sqlstate '22003' set message_text = @errorMsg ;
	end if;
END//

DELIMITER ;

DELIMITER //

CREATE TRIGGER developers.controllo_update_skill_tipologia_corretta_task
before update on developers.task_skill
for each row
BEGIN
	SET @tipo_task = (SELECT task.tipologia FROM developers.task WHERE task.id = new.task);
	if (new.skill) NOT IN (SELECT tipo_task_skill.skill FROM developers.tipo_task_skill WHERE tipo_task_skill.tipologia_task = @tipo_task)	then 
		SET @errorMsg = CONCAT ('La skill ', new.skill, ' selezionata non va bene per il tipo di task ' , @tipo_task, 'per il task numero ', new.task);
		signal sqlstate '22003' set message_text = @errorMsg ;
	end if;
END//

DELIMITER ;

DELIMITER //

CREATE TRIGGER developers.controllo_inserimento_sviluppatore_task_concluso
before insert on developers.sviluppatore_task
for each row
BEGIN
	if 
		(SELECT stato_task FROM developers.task WHERE task.id = new.task) = false
	then
		signal sqlstate '22003' set message_text = 'Il task è già concluso non puoi aggiungere collaboratori.';
	end if;
END//

DELIMITER ;

DELIMITER //

CREATE TRIGGER developers.controllo_update_sviluppatore_task_concluso
before update on developers.sviluppatore_task
for each row
BEGIN
	if 
		(SELECT stato_task FROM developers.task WHERE task.id = new.task) = false
	then
		signal sqlstate '22003' set message_text = 'Il task è già concluso non puoi aggiungere collaboratori.';
	end if;
END//

DELIMITER ;

/**
	inserimento dei dati nelle varie tabelle
**/

INSERT INTO developers.sviluppatore VALUES 
	(1,'Mario','Rossi','RSSMRI81P21B123C','1981-12-12', 'Via Nazionale 129, Milano (MI) 20142','m.rossi@gmail.com', NULL, 3803333333,'Esempio cv', NULL);

INSERT INTO developers.sviluppatore VALUES 
	(2,'Maria','Rossi','RSSMRI94P21B123C','1994-08-20','Via Torino, Milano (MI) 20144','maria.rossi@gmail.com', NULL,3453333333,'Esempio cv',NULL),
    (3,'Fabrizio','Verdi','FSSMRI94P21B123C','1990-11-10','Via Luchelli 129, Milano (MI) 20142','f.verdi@gmail.com', NULL,3893333333,'Esempio cv',NULL),
    (4,'Andrea','Blu','NDRAI91P21B123C','1999-12-12','Via A. Bianchi 129, Milano (MI) 20142','a.blu@gmail.com', NULL, 3333333333,'Esempio cv', NULL);

INSERT INTO developers.skill VALUES 
(1,'Java'),
(2,'Python'),
(3,'SQL'),
(4,'NOSql'),
(5,'Javascript'),
(6,'CSS'),
(7,'Photoshop'),
(8,'Figma'),
(9,'Excel'),
(10,'PowerPoint');

INSERT INTO developers.sviluppatore_skill VALUES
(8,1,1),
(5,1,6),
(10,1,2),
(7,2,3),
(8,2,9),
(3,2,5),
(9,3,8),
(7,3,7),
(10,4,10),
(1,4,1),
(1,4,3);

INSERT INTO developers.tipologia_task VALUES
(1,'sviluppo'),
(2, 'ux'),
(3,'test'),
(4,'sal'),
(5,'bugfix');

INSERT INTO developers.tipo_task_skill VALUES
(1,1),
(1,2),
(1,3),
(1,4),
(1,5),
(1,6),
(2,6),
(2,7),
(2,8),
(3,9),
(4,9),
(4,10),
(5,1),
(5,2),
(5,3),
(5,4),
(5,5),
(5,6);

INSERT INTO developers.progetto VALUE
(1, 'creazione e-commerce',1),
(2, 'chat bot', 1),
(3, 'crm cliente',3);

INSERT INTO developers.messaggio VALUES
(1, true, 'richiesta', 'aiutatemi a risolvere...', 1,1),
(2, true, 'annuncio', 'vogliamo annunciare a tutto il personale che...', 3,2),
(3, false, 'chat', 'ciao volevo dirti che...', 4,1);

INSERT INTO developers.task VALUE
(1, 2, '2024-01-12', '2024-12-31','sviluppo homepage','....',true,1,1),
(2, 3, '2024-01-20', '2024-12-31','grafica dashboard','....',true,2,1),
(3, 2, '2024-01-22', '2024-12-31','elaborazione database','....',true,1,1),
(4, 2, '2024-01-17', '2024-12-31','test sprint 1','....',true,3,2),
(5, 4, '2024-01-08', '2024-12-31','meeting con cliente','....',true,4,2),
(6, 6, '2024-01-30', '2024-12-31','raccolta requisiti','....',true,4,2),
(7, 5, '2024-02-10', '2024-12-31','correzione query in produzione','....',true,5,3),
(8, 1, '2024-03-12', '2024-12-31','supporto cliente','....',true,1,3),
(9, 1, '2023-01-12', '2023-12-31','modifica logo app','....',false,2,3),
(10, 3, '2023-01-12', '2023-12-31','fix colore pulsanti home','....',false,5,3);

UPDATE developers.task SET stato_task = true WHERE stato_task = false;

INSERT INTO developers.task_skill VALUE
(4,1,1),(4,1,2),(4,1,3),(4,1,4),
(4,2,7),(4,2,8),
(4,3,3),(4,3,4),(4,3,2),
(4,4,9),
(4,5,9),(4,5,10),
(4,6,9),(4,6,10),
(4,7,3),(4,7,4),
(4,8,5),(4,8,1),(4,8,2),
(4,9,7),(4,9,8),
(4,10,1),(4,10,2),(4,10,3);

INSERT INTO developers.sviluppatore_task VALUE
(NULL,1,1),(NULL,1,2),
(NULL,2,3),(NULL,2,4),
(NULL,3,1),
(NULL,4,3),(NULL,4,2),
(NULL,5,2),(NULL,5,3),
(NULL,6,1),(NULL,6,2),(NULL,6,3),(NULL,6,4),
(NULL,7,2),(NULL,7,4),
(NULL,8,1),
(4,9,2),
(5,10,1),(5,10,3);

UPDATE developers.task SET stato_task = false WHERE id = 9 or id = 10;

INSERT INTO developers.allegato VALUE
(1,'Allegato1','PDF','2024-04-14','',1,1);

INSERT INTO developers.allegato VALUE
(2,'Allegato1','PDF','2024-04-18','',2,1),
(3,'Allegato2','EXCEL','2023-02-18','',4,2),
(4,'Allegato2','EXCEL','2021-06-30','',3,2),
(5,'Allegato3','WORD','2024-02-26','',1,3),
(6,'Allegato4','PP','2024-03-03','',4,4),
(7,'Allegato5','WORD','2024-04-04','',1,5),
(8,'Allegato6','PDF','2024-04-18','',2,5),
(9,'Allegato7','IMG','2024-01-30','',3,6),
(10,'Allegato7','IMG','2024-01-22','',2,6),
(11,'Allegato8','PDF','2024-02-02','',1,7),
(12,'Allegato9','WORD','2024-04-07','',1,8),
(13,'Allegato10','PP','2024-03-07','',2,9),
(14,'Allegato11','PDF','2024-04-18','',3,10);

/**
	svolgimento delle varie operazioni richieste
**/

-- esercizio 1 --
SELECT distinct (nome_progetto) FROM developers.progetto
JOIN developers.task ON task.progetto = progetto.id
WHERE task.stato_task = true;

-- esercizio 2 --
INSERT INTO developers.progetto VALUE
(4, 'sviluppo gestionale',4);

INSERT INTO developers.task VALUE
(11, 2, '2024-01-01', '2024-12-31','analisi schema er','....',true,1,4);

-- esercizio 3 --
-- per questo esercizio ho valutato 2 modi di farlo --

-- questo caso mi darà tutta la lista di progetti, non distinti, dal più recente al meno recente --
SELECT nome_progetto, allegato.data_modifica FROM developers.progetto
JOIN developers.task ON task.progetto = progetto.id
JOIN developers.allegato ON allegato.task = task.id
ORDER BY allegato.data_modifica DESC;

-- questo caso mi darà la lista dei soli progetti con file modificato nell'ultimo mese --
SELECT distinct (nome_progetto) FROM developers.progetto
JOIN developers.task ON task.progetto = progetto.id
JOIN developers.allegato ON allegato.task = task.id
WHERE allegato.data_modifica >= DATE_SUB(now(), INTERVAL 1 month);

-- esercizio 4 --
SELECT distinct (nome_progetto) FROM developers.progetto
JOIN developers.task ON task.progetto = progetto.id
JOIN developers.allegato ON allegato.task = task.id
WHERE allegato.data_modifica <= DATE_SUB(now(), INTERVAL 1 year) and task.stato_task = true;

-- esercizio 5 --
SELECT nome,cognome FROM developers.sviluppatore
JOIN developers.sviluppatore_skill ON sviluppatore.id = sviluppatore_skill.sviluppatore
WHERE sviluppatore_skill.skill IN (1,3,4,10) and sviluppatore_skill.livello_competenza > 3;

-- esercizio 6 --
SELECT distinct (nome_progetto) FROM developers.progetto
WHERE nome_progetto LIKE '%bot%';

-- esercizio 7 --
INSERT INTO developers.sviluppatore VALUES 
	(5,'Sara','Bianchi','SSSMRI81P21B123C','1997-12-10', 'Via Nazionale 129, Milano (MI) 20142','s.bianchi@gmail.com', 0288945621, 3003333333,'Esempio cv', NULL);
    
-- esercizio 8 --
INSERT INTO developers.sviluppatore_skill VALUES
(9,5,7);

UPDATE developers.sviluppatore_skill SET sviluppatore_skill.livello_competenza = 10
WHERE sviluppatore = 5 and skill = 7;

-- esercizio 9 --
UPDATE developers.sviluppatore_task SET valutazione = 4
WHERE sviluppatore = 1 and task =1;

-- esercizio 10 --
SELECT * FROM developers.progetto 
JOIN developers.task ON task.progetto = progetto.id
JOIN developers.sviluppatore_task ON task.id = sviluppatore_task.task
WHERE progetto.coordinatore = 1 or sviluppatore_task.sviluppatore = 1;

-- esercizio 11 --
UPDATE developers.task SET data_fine = '2024-11-30'
WHERE id = 1;

-- esercizio 12 --
DELETE FROM developers.sviluppatore_task
WHERE sviluppatore = 1 and task = 6;

-- esercizio 13 --
-- poichè limit non è supportato all'interno di una subquery con clausola IN, la aggiro inserendo il valore della subquery in una altra query ma senza il limit --
DELETE FROM developers.allegato
WHERE id IN (SELECT * FROM (SELECT id FROM developers.allegato WHERE nome_file = 'Allegato1' ORDER BY data_modifica DESC limit 1) as t1);

-- esercizio 14 --
SELECT nome_file, data_modifica, nome , cognome FROM developers.allegato
JOIN developers.sviluppatore ON  sviluppatore.id = allegato.modificato_da
WHERE nome_file = 'Allegato2'
ORDER BY data_modifica DESC;






